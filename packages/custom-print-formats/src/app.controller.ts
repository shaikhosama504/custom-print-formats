import {
  Body,
  Controller,
  NotFoundException,
  Post,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AppService } from './app.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const carbone = require('carbone');
import { Response } from 'express';
import { existsSync } from 'fs';
import { IsNotEmpty, IsNotEmptyObject, IsString } from 'class-validator';

export class Payload {
  @IsNotEmpty()
  @IsString()
  fileName: string;

  @IsNotEmpty()
  @IsString()
  templateName: string;

  @IsNotEmpty()
  @IsNotEmptyObject()
  templateData: any;
}

export const TEMPLATE_PATH = '/opt/print/templates/';
export const options = {
  convertTo: 'pdf', //can be docx, txt, ...
};

@Controller('print-format')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('v1/download')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async getPdf(@Body() payload: Payload, @Res() response: Response) {
    const template = TEMPLATE_PATH + payload?.templateName;

    if (!existsSync(template)) {
      throw new NotFoundException('File Not Found!');
    }

    try {
      await carbone.render(
        template,
        payload?.templateData,
        options,
        (err, result) => {
          if (err) throw new Error(err);
          response.set({
            'Content-Type': 'application/pdf',
            'Content-Disposition': `attachment; filename=${payload?.fileName}`,
            'Content-Length': result.length,
          });
          response.end(result);
        },
      );
    } catch (err) {
      throw new Error(err);
    }
  }
}
